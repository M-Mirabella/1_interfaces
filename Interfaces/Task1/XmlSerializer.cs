﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

namespace Task1
{
    class XmlSerializer<T> : ISerializer<T>
    {
        private static readonly XmlWriterSettings xmlWriterSettings;

        static XmlSerializer()
        {
            xmlWriterSettings = new XmlWriterSettings{ Indent = true};
        }

        public T Deserialize<T>(String content)
        {
            return Deserialize<T>(content);
        }

        public string Serialize<T>(T item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
                .UseAutoFormatting()
                .UseOptimizedNamespaces()
                .EnableImplicitTyping(typeof(T))
                .Create();

            return serializer.Serialize(xmlWriterSettings, item);
        }
    }
}
