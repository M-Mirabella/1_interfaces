﻿
namespace Task1
{
    internal interface ISerializer<T>
    {
        string Serialize<T>(T item);
        T Deserialize<T>(string data);

    }
}