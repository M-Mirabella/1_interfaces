﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Linq;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Person[] p = new Person[4];

            p[0] = new Person { Age = 37, FirstName = "Ivan", LastName = "Petrov" };
            p[1] = new Person { Age = 19, FirstName = "Alex", LastName = "Maksimov" };
            p[2] = new Person { Age = 75, FirstName = "Dmitry", LastName = "Sitkin" };
            p[3] = new Person { Age = 24, FirstName = "Elena", LastName = "Basaley" };

            XmlSerializer<Person> serializer = new XmlSerializer<Person>();
            String contents = serializer.Serialize<Person[]>(p);

            List<Person> lp = new List<Person>();
            StreamReader<Person> str = new StreamReader<Person>(contents);
            foreach(Person p1 in str)
            {
                lp.Add(p1);
            }
          
            PersonSorter pSorter = new PersonSorter();
            p = pSorter.Sort(p).ToArray();
            foreach (Person ps in p)
            {
                Console.WriteLine(ps.FirstName);
            }            

            Console.ReadKey();
        }
    }
}
