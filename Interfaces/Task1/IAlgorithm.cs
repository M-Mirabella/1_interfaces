﻿using System.Collections.Generic;

namespace Task1
{
    interface IAlgorithm<T>
    {
        IEnumerable<T> Sort(IEnumerable<T> notSortedItems);
    }
}
