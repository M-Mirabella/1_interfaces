﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task1
{
    class PersonSorter : IAlgorithm<Person>
    {
        public IEnumerable<Person> Sort(IEnumerable<Person> notSortedItems)
        {
            var sortedItems = from u in notSortedItems
                              orderby u.FirstName
                              select u;

            return sortedItems;

        }

    }
}
