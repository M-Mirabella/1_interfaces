﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Task1
{
    class StreamReader<T> : IEnumerable<T>
    {        
        private string content;
        IExtendedXmlSerializer serializer;

        public StreamReader(string xml)
        {
            this.content = xml;

            serializer = new ConfigurationContainer()
                                .UseAutoFormatting()
                                .UseOptimizedNamespaces()
                                .EnableImplicitTyping(typeof(T))
                                .Create();
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            T[] p2 = serializer.Deserialize<T[]>(content);

            for (int i = 0; i < p2.Length; i++)
            {
                yield return p2[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => throw new NotImplementedException();
        
    }
}
