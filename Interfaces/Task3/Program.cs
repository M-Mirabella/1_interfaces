﻿using System;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {

            Account acc1 = new Account { BirthDate = new DateTime(1990, 10, 4), FirstName = "Alex", LastName = "Maneev" };
            Account acc2 = new Account { BirthDate = new DateTime(2020, 10, 4), FirstName = "Ivan", LastName = "Bunin" };
            Account acc3 = new Account { BirthDate = new DateTime(1983, 12, 8), FirstName = "Elena", LastName = "Ivanova" };
            Account acc4 = new Account { BirthDate = new DateTime(1951, 07, 24), FirstName = "Egor", LastName = "Sitkin" };


            Repository repository = new Repository();

            AccountService accountService = new AccountService(repository);
            accountService.AddAccount(acc1);
            accountService.AddAccount(acc2);
            accountService.AddAccount(acc3);
            accountService.AddAccount(acc4);
            
            Account acc5 = repository.GetOne(x => x.LastName == "Maneev");
            Account acc6 = repository.GetOne(x => x.BirthDate.Year < 1985);

            var allAccounts = repository.GetAll();

    
        }
    }

}
