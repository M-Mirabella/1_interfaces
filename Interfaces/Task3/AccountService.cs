﻿using System;

namespace Task3
{
    public class AccountService : IAccountService
    {
        private IRepository<Account> _repository;
        public AccountService(IRepository<Account> r)
        {
            _repository = r;
        }

        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        public void AddAccount(Account account)
        {
            if (AccValidation(account) == true)
            {
               _repository.Add(account);
            }                 
        }

    
        private bool AccValidation(Account account)
        {
            return account.FirstName.Length < 2 || account.LastName.Length < 2 || account.BirthDate > DateTime.Today.AddYears(-18);           
        }
    }
}
