﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Task3
{
    public class Repository: IRepository<Account>
    {

        private XmlSerializer _formatter = new XmlSerializer(typeof(List<Account>));
        private static string _fileName = "Accounts.xml";

        public void Add(Account item)
        {
            List<Account> accTemp = new List<Account>();
            if (File.Exists(_fileName))
            {
                accTemp = GetAll().ToList();

            }
            accTemp.Add(item);

            using (StreamWriter sw = new StreamWriter(_fileName, false))
            {
                _formatter.Serialize(sw, accTemp);

            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            using (StreamReader sr = new StreamReader(_fileName))
            {
                IEnumerable<Account> accTemp = (IEnumerable<Account>)_formatter.Deserialize(sr);

                return accTemp.Where(predicate).FirstOrDefault();

            }
        }

        public IEnumerable<Account> GetAll()
        {
            using (StreamReader sr = new StreamReader(_fileName))
            {
                foreach (Account acc in (List<Account>)_formatter.Deserialize(sr))
                {
                    yield return acc;
                }
            }

        }

    }
}
