﻿using Moq;
using System;
using System.Collections.Generic;
using Task3;
using Xunit;

namespace XUnitTestProject
{
    public class AccountServiceTests
    {
        [Fact]
        public void AddAccountAddsAccountIntoRepository()
        {
            Mock<IRepository<Account>> mock = new Mock<IRepository<Account>>();
            mock.Setup(repo => repo.GetAll()).Returns(GetTestUsers());
            AccountService accService = new AccountService(mock.Object);

            foreach(Account acc in GetTestUsers())
            {
                accService.AddAccount(acc);
            }

            var result = mock.Object.GetAll();

            Assert.NotNull(result);
            var viewResult = Assert.IsType<List<Account>>(result);
        }

        private List<Account> GetTestUsers()
        {
            List<Account> accounts = new List<Account>
            {
                new Account { BirthDate = new DateTime(1958, 8, 28), FirstName = "Tim", LastName = "Burton" },
                new Account { BirthDate = new DateTime(1995, 5, 25), FirstName = "Jhon", LastName = "Black" },
                new Account { BirthDate = new DateTime(1915, 12, 12), FirstName = "Frank", LastName = "Sinatra" }
            };
            return accounts;
        }
    }
}
